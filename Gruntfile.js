/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? " * " + pkg.homepage + "\\n" : "" %>' +
      ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;\n' +
      ' */\n',
    dirs: {
      input: 'src',
      working: 'build',
      output: 'pub'
    },
    // Task configuration.
    copy: {
      dist: {
        files: [{
            expand: true,
            cwd: '<%= dirs.input %>/assets/',
            src: ['**'],
            dest: '<%= dirs.output %>'
        }]
      }
    },
    concat: {
      options: {
        banner: '<%= banner %>',
        stripBanners: true
      },
      dist_js: {
        src: ['<%= dirs.input %>/js/*.js'],
        dest: '<%= dirs.working %>/<%= pkg.name %>.js'
      },
      dist_css: {
        src: ['<%= dirs.working %>/css/*.css'],
        dest: '<%= dirs.working %>/<%= pkg.name %>.css'
      }
    },
    // Javascript
    uglify: {
      options: {
        banner: '<%= banner %>'
      },
      dist: {
        src: '<%= concat.dist_js.dest %>',
        dest: '<%= dirs.output %>/<%= pkg.name %>.min.js'
      }
    },
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        unused: true,
        boss: true,
        eqnull: true,
        browser: true,
        devel: true,
        globals: {}
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      dist: {
        src: ['<%= dirs.input %>/**/*.js']
      }
    },
    // CSS
    compass: {
      dist: {
        options: {
          sassDir: '<%= dirs.input %>/scss',
          cssDir: '<%= dirs.working %>/css',
          environment: 'production'
        }
      }
    },
    cssmin: {
      dist: {
        options: {
          roundingPrecision: -1
        },
        src: '<%= concat.dist_css.dest %>',
        dest: '<%= dirs.output %>/<%= pkg.name %>.min.css'
      }
    },
    // HTML
    pug: {
      dist: {
        options: {
          pretty: true,
          data: {
            cssPath: '/<%= pkg.name %>.min.css',
            jsPath: '/<%= pkg.name %>.min.js',
            thisYear: grunt.template.today('yyyy')
          }
        },
        files: [{
          expand: true,
          cwd: '<%= dirs.input %>/pug/',
          src: ['**/*.pug'],
          dest: '<%= dirs.working %>/html/',
          ext: '.html'
        }]
      }
    },
    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        files: [{
          expand: true,
          cwd: '<%= dirs.working %>/html/',
          src: ['**/*.html'],
          dest: '<%= dirs.output %>/',
          //ext: '.html'
        }]
      },
    },
    // Misc
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      dist: {
        files: '<%= dirs.input %>/**',
        tasks: ['default']
      }
    },
    clean: {
      working: {
        src: ['<%= dirs.working %>']
      },
      output: {
        src: ['<%= dirs.output %>']
      }
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-pug');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');

  // Tasks.
  grunt.registerTask('test', ['jshint']);
  grunt.registerTask('js', ['jshint', 'concat', 'uglify']);
  grunt.registerTask('css', ['compass', 'cssmin']);
  grunt.registerTask('html', ['pug', 'htmlmin']);
  grunt.registerTask('dist', ['js', 'css', 'html']);
  // Default task.
  grunt.registerTask('default', ['clean', 'dist', 'watch']);
};
