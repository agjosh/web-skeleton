

Installation
============

Install node.js from http://nodejs.com/
Install grunt:

    npm install -g grunt-cli

Install ruby from http://www.ruby-lang.org/
Install bundler:

    gem install bundler

Install packages:

    bundle install
    npm install


Usage
=====

Web source goes in src/:

- pug/ for pug html templates (see http://pugjs.com/)
- scss/ for sass stylesheets (see http://sass-lang.com/ also inlcudes compass http://compass-style.org/)
- js/ for javascript scripts

 Build by running

    grunt

Clean output by running

    grunt clean
